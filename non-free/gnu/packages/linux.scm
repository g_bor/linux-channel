(define-module (non-free gnu packages linux)
  #:use-module (gnu packages linux)
  #:use-module (guix download)
  #:use-module (guix packages))

(define-public linux-non-free
  (package
   (inherit linux-libre)
   (name "linux-non-free")
   (version "5.14.7")
   (source (origin
	    (method url-fetch)
	    (uri (string-append "https://cdn.kernel.org/pub/linux/kernel/"
				"v5.x/linux-" version ".tar.xz"))
	    (sha256
	     (base32
	      "1avypasvic298823xzpzzkjbmfv9s8bjnmq92ri62qbakx23j9dg"))))))

linux-non-free
